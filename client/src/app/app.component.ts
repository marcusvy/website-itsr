import { Component } from '@angular/core';

@Component({
  selector: 'itsr-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'itsr';
}
