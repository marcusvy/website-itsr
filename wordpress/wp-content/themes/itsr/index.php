<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package itsr
 */
//arquivo padrão
$file = 'dist/index.html';
$seo = 'seo.html';

if(file_exists($file)){
    $distFile = file_get_contents($file, true);
    $distFile = str_replace('<link href="', sprintf('<link href="%s/dist/', get_template_directory_uri()), $distFile);
    $distFile = str_replace('<script type="text/javascript" src="', sprintf('<script type="text/javascript" src="%s/dist/', get_template_directory_uri()), $distFile);
    echo $distFile;
}else{
    include "protected.html";
}