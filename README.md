# ITSR Website

Wordpress with Angular theme

## Author

**Adm. Marcus Vinícius R G Cardoso**
(CEO & Founder Mvinicius Consultoria)

- E-mail: <mailto:marcus@mviniciusconsultoria.com.br>
- Site: <http://mviniciusconsultoria.com.br>

## Copyright an Licence

Always free and open source. Be free to study, modify and share.

2013-2017 MVinicius Consultoria, by
[MIT License](http://opensource.org/licenses/MIT).
Documentation under [MIT Licence](http://opensource.org/licenses/MIT).